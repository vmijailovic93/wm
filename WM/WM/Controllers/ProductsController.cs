﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WM_API.API;
using WM_API;
using WM_API.Models;

namespace WM.Controllers
{
    public class ProductsController : Controller
    {
        #region Products_Db
        public ActionResult ProductsDb()
        {
            List<Product> _allProducts = ProductsService.GetProductsFromDb();
            return View("ProductsDb", _allProducts);
        }

        public PartialViewResult ProductDetails()
        {
            return PartialView("_AddProduct");
        }

        [HttpPost]
        public ActionResult Create(ProductModel model)
        {
            ProductsService.Insert(model);
            return RedirectToAction("ProductsDb");
        }

        public PartialViewResult Delete(int id)
        {
            var productToDelete = ProductsService.GetProductById(id);
            return PartialView("_DeleteProduct", productToDelete);
        }

        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                ProductsService.DeleteProductById(id);
                return RedirectToAction("Index","Home");
            }   
            catch
            {
                return View();
            }
        }

        public PartialViewResult Update(int id)
        {
            var result = ProductsService.GetProductById(id);
            return PartialView("_UpdateProduct", result);
        }


        [HttpPost]
        public ActionResult Edit(int id, ProductModel model)
        {
            ProductsService.Update(model);
            return RedirectToAction("ProductsDb");
        }
        #endregion


        
    }
}
