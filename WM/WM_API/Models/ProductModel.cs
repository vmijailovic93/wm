﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WM_API.Models
{
    public class ProductModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }

        public string Manufacturer { get; set; }

        public string Supplier { get; set; }

        public float Price { get; set; }
    }
}