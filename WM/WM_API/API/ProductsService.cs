﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WM_API.Models;
using System.Data.Entity.Migrations;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;

namespace WM_API.API
{
    public class ProductsService
    {
        #region Products_Db
        public  static List<Product> GetProductsFromDb()
        {
            using (var db = new WMDbContext())
            {
                return db.Products.ToList();
            }
        }

        public static Product GetProductById(int id)
        {
            using (var db = new WMDbContext())
            {
                return db.Products.FirstOrDefault(p => p.Id == id);
            }
        }

        public static void DeleteProductById(int id)
        {
            using (var db = new WMDbContext())
            {
                var productToDelete = db.Products.FirstOrDefault(p => p.Id == id);
                if (productToDelete != null)
                {
                    db.Products.Remove(productToDelete);
                    try {
                        db.SaveChanges();
                    }
                    catch(Exception ex)
                    {
                    }
                    
                }
            }
        }
        
        public static void Insert(ProductModel model)
        {
            using (var db = new WMDbContext())
            {
                Product _item = new Product();

                _item.Name = model.Name;
                _item.Description = model.Description;
                _item.Category = model.Category;
                _item.Manufacturer = model.Manufacturer;
                _item.Supplier = model.Supplier;
                _item.Price = model.Price;
                db.Products.Add(_item);
                db.SaveChanges();
            }
        }

        public static void Update(ProductModel model)
        {
            using (var db = new WMDbContext())
            {
                var productToUpdate = db.Products.FirstOrDefault(x => x.Id == model.Id);

                if (productToUpdate == null)
                    return;

                productToUpdate.Name = model.Name;
                productToUpdate.Description = model.Description;
                productToUpdate.Category = model.Category;
                productToUpdate.Manufacturer = model.Manufacturer;
                productToUpdate.Supplier = model.Supplier;
                productToUpdate.Price = model.Price;
                db.Products.AddOrUpdate(productToUpdate);
                db.SaveChanges();
            }
        }
        #endregion

        
    }
}
